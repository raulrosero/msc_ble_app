package ipleiria.pt.msc_app;


import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends Activity implements SensorEventListener {
    private static final int REQUEST_ENABLE_BT = 1 ;
    //----------Objects for Bluetooth--------------------//
    Button turnOn,getVisible,turnOff, listB;
    private BluetoothAdapter BA;
    private Set<BluetoothDevice> pairedDevices;
    private BluetoothDevice device = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;
    private boolean bluetoothConnected= false;
    private static final UUID MY_UUID =UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    ListView lv;
    //----------End objects for Bluetooth--------------------//
    //----------Objects for Accelerometer--------------------//
    private float lastZ, lastY;
    private SensorManager sensorManager;
    private Sensor gyroscope;
    private Sensor accelerometer;
    private float deltaZ = 0;
    private float deltaY = 0;
    private float vibrateThreshold = 0;
    private long lastUpdate = 0;
    private TextView currentX, currentY ,count;
    int cont=0;
    String [] direction = {"NONE","NONE"};
    public Vibrator v;
    //----------End Objects for Accelerometer--------------------//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //-----------------Bluetooth--------------//
        setContentView(R.layout.activity_main);
        turnOn = findViewById(R.id.turnOn);
        getVisible=findViewById(R.id.getVisible);
        turnOff=findViewById(R.id.turnOff);
        listB=findViewById(R.id.list);
        BA = BluetoothAdapter.getDefaultAdapter();
        lv = findViewById(R.id.listView);
        //--------------- End Bluetooth--------------//

        initializeViews();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null) {
            // success! we have an accelerometer
            gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            vibrateThreshold = gyroscope.getMaximumRange() / 2;
        }
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
        CheckBTState();
        //initialize vibration
        v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
    }



    public void initializeViews() {
        currentX = findViewById(R.id.currentX);
        currentY = findViewById(R.id.currentY);
        count=findViewById(R.id.count);
    }
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, accelerometer,SensorManager.SENSOR_DELAY_NORMAL);
    }
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        displayCleanValues();
        displayCurrentValues();
        long curTime = System.currentTimeMillis();
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            if ((curTime - lastUpdate) > 200) {
                lastUpdate = curTime;
                deltaY = lastY - event.values[1];
                lastY = event.values[1];
                if (deltaY > 2){
                    direction[0] = "PLAY";
                    cont++;
                    v.vibrate(100);
                    if(bluetoothConnected)
                    onSensorChanged(1);
                }
                else if (deltaY < -2){
                    direction[0] = "PAUSE";
                    cont++;
                    v.vibrate(100);
                    if(bluetoothConnected)
                    onSensorChanged(2);
                }
            }
        }
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if ((curTime - lastUpdate) > 200) {
                lastUpdate = curTime;
                deltaZ = lastZ - event.values[2];
                lastZ = event.values[2];
                if (deltaZ > 2) {
                    direction[1] = "VOL UP";
                    cont++;
                    v.vibrate(100);
                    if(bluetoothConnected)
                    onSensorChanged(3);
                } else if (deltaZ < -2) {
                    direction[1] = "VOL DOWN";
                    cont++;
                    v.vibrate(100);
                    if(bluetoothConnected)
                    onSensorChanged(4);
                }
            }
        }
    }

    public void displayCleanValues() {
        currentX.setText(direction[0]);
        currentY.setText(direction[1]);
        count.setText(cont+"");
    }

    // display the current x,y,z accelerometer values
    public void displayCurrentValues() {
        currentX.setText(direction[0]);
        currentY.setText(direction[1]);
        count.setText(cont+"");
    }

    public void on(View view) {
        if (!BA.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 0);
            Toast.makeText(getApplicationContext(), "Turned on",Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(), "Already on", Toast.LENGTH_LONG).show();
        }
    }

    public void visible(View view) {
        Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        startActivityForResult(getVisible, 0);
    }

    public void off(View view) {
        if (BA.isEnabled()) {
            BA.disable();
            bluetoothConnected=false;
            Toast.makeText(getApplicationContext(), "Turned off", Toast.LENGTH_LONG).show();
        }
    }
    public void list(View v){
        pairedDevices = BA.getBondedDevices();
        ArrayList list = new ArrayList();
        for(BluetoothDevice bt : pairedDevices)
        {
            list.add(bt.getName()+"/"+bt.getAddress());
        }
        list.remove(0);
        Toast.makeText(getApplicationContext(), "Showing Paired Devices",Toast.LENGTH_SHORT).show();
        final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, list);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {

                String item = ((TextView)view).getText().toString();
                item=item.substring(item.indexOf("/")+1,item.length());
                device = BA.getRemoteDevice(item);
                try {
                    btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
                } catch (IOException e) {
                    AlertBox("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
                }

                // Discovery is resource intensive.  Make sure it isn't going on
                // when you attempt to connect and pass your message.
                BA.cancelDiscovery();

                // Establish the connection.  This will block until it connects.
                try {
                    btSocket.connect();
                } catch (IOException e) {
                    try {
                        btSocket.close();
                    } catch (IOException e2) {
                        AlertBox("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
                    }
                }
                try {
                    outStream = btSocket.getOutputStream();
                    bluetoothConnected=true;
                } catch (IOException e) {
                    AlertBox("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
                }
                Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
            }
        });
    }
    private void CheckBTState() {
        if(BA==null) {
            AlertBox("Fatal Error", "Bluetooth Not supported. Aborting.");
        } else {
            if (BA.isEnabled()) {
                count.setText("\n...Bluetooth is enabled...");
            } else {
                //Prompt user to turn on Bluetooth
                Intent enableBtIntent = new Intent(BA.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    public void AlertBox( String title, String message ){
        new AlertDialog.Builder(this)
                .setTitle( title )
                .setMessage( message + " Press OK to exit." )
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).show();
    }
    public void onSensorChanged(Integer message) {
        try {
            outStream = btSocket.getOutputStream();
        } catch (IOException e) {
            AlertBox("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
        }
        try {
            outStream.write(message);
        } catch (IOException e) {
            String msg = "In onResume() and an exception occurred during write: " + e.getMessage();
            if (BA.getAddress().equals("00:00:00:00:00:00"))
                msg = msg + ".\n\nUpdate your server address from 00:00:00:00:00:00 to the correct address on line 37 in the java code";
            msg = msg +  ".\n\nCheck that the SPP UUID: " + MY_UUID.toString() + " exists on server.\n\n";

            AlertBox("Fatal Error", msg);
        }
    }
}

